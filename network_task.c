/*
 * network_task.c
 *
 *  Created on: Nov 24, 2020
 *      Author: alex
 */


#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>

#include <netdb.h>

#include <ti/net/slnetutils.h>

//#include <ti/ndk/inc/netmain.h>
#include <ti/ndk/inc/os/oskern.h>

#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>

#include "console.h"
#include "temperature.h"


// Yep... this is how they do it in the tcpEcho example...
extern void fdOpenSession();
extern void fdCloseSession();
extern void *TaskSelf();



void tcp_echo_connection_thread(int16_t socket, uintptr_t unused) {
    int16_t connection_socket = socket;
    char recv_queue[64];
    int recv_size;

//    fdOpenSession(parent_task);
    fdOpenSession(TaskSelf());


    while((recv_size = recv(connection_socket, recv_queue,
                            sizeof(recv_queue), 0)) > 0) {
        if(send(connection_socket, recv_queue, recv_size, 0) < 0) {
            break;
        }
    }

    close(connection_socket);

    fdCloseSession(TaskSelf());

    //TaskYield();
}

struct listener_info_s {
    void (* connection_thread_func)(int16_t socket, uintptr_t unused);
    uint16_t port;
};

void listener_thread(struct listener_info_s* listener_info,
                              char* connection_thread_name) {
    Task_Params params;
    Task_Handle ndkThread;

    int socket_;
    int connection_socket;
    volatile int error;
    struct sockaddr_in sock_address;
    struct sockaddr_in connection_address;
    socklen_t connection_address_size;

    sock_address.sin_family = AF_INET;
    sock_address.sin_addr.s_addr = htonl(INADDR_ANY);
    sock_address.sin_port = htons(listener_info->port);

    fdOpenSession(TaskSelf());

    socket_ = socket(AF_INET, SOCK_STREAM ,IPPROTO_TCP);
    bind(socket_, (struct sockaddr *)&sock_address, sizeof(sock_address));
    listen(socket_, 3);
    //error = setsockopt(socket, SOL_SOCKET, SO_KEEPALIVE, &optval, optlen);

    while(1) {
        connection_address_size = sizeof(connection_address);
        connection_socket = accept(socket_,
                                   (struct sockaddr *)&connection_address,
                                   &connection_address_size);
        if(connection_socket == -1) {
            Task_sleep(1000);
            //error = fdError();
            //break;
            continue;
        }

        Task_Params_init(&params);
        params.instance->name = connection_thread_name;
        params.priority = 4;
        params.stackSize = 2048;
        params.arg0 = connection_socket;

        ndkThread = Task_create((Task_FuncPtr)listener_info->connection_thread_func, &params, NULL);


        // OS_TASKPRINORM = 4
        //TaskCreate(listener_info->connection_thread_func, connection_thread_name,
        //           4, 2048, (uintptr_t) connection_socket, 0, 0);
    }

    fdCloseSession(TaskSelf());
}

struct listener_info_s tcp_echo_listener = {
    .connection_thread_func = tcp_echo_connection_thread,
    .port = 1337
};
struct listener_info_s console_listener = {
    .connection_thread_func = console_task,
    .port = 42
};
struct listener_info_s temperature_listener = {
    .connection_thread_func = temperature_thread,
    .port = 451
};
