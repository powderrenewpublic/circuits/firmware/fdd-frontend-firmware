/*
 * lna.c
 *
 *  Created on: Sep 6, 2019
 *      Author: Alex Orange
 */

#include <ti/devices/msp432e4/driverlib/sysctl.h>
#include <ti/devices/msp432e4/driverlib/gpio.h>

#include <ti/devices/msp432e4/inc/msp432e401y.h>


uint_fast8_t lna_bias_last;
uint_fast8_t lna_mode_enabled;

void setup_lna() {
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);

    // Power supply GPIO
    GPIODirModeSet(GPIO_PORTA_BASE, 0x0f, GPIO_DIR_MODE_OUT);
    GPIODirModeSet(GPIO_PORTA_BASE, 0x30, GPIO_DIR_MODE_IN);
    GPIOPadConfigSet(GPIO_PORTA_BASE, 0x0f, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD);
    GPIOPinWrite(GPIO_PORTA_BASE, 0x08, 0x08);

    // LNA bias GPIO
    lna_bias_last = 0x08;
    lna_mode_enabled = 1;
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOK);
    GPIODirModeSet(GPIO_PORTK_BASE, 0x07, GPIO_DIR_MODE_IN);
    GPIODirModeSet(GPIO_PORTK_BASE, 0x08, GPIO_DIR_MODE_OUT);
    GPIOPadConfigSet(GPIO_PORTK_BASE, 0x0F, GPIO_STRENGTH_12MA, GPIO_PIN_TYPE_STD);
}

void set_lna_power_state(uint_fast8_t on) {
    GPIOPinWrite(GPIO_PORTA_BASE, 0x01, on ? 0x01 : 0x00);
}

void set_lna_voltage(uint_fast8_t value) {
    GPIOPinWrite(GPIO_PORTA_BASE, 0x02, (value & 0x01) ? 0x02 : 0x00);
    GPIOPinWrite(GPIO_PORTA_BASE, 0x04, (value & 0x02) ? 0x04 : 0x00);
}

uint_fast8_t get_lna_power_good() {
    return GPIOPinRead(GPIO_PORTA_BASE, 0x30);
}

const uint8_t lna_flip_bits[] = {
    0x00, // 0x00
    0x08, // 0x01
    0x04, // 0x02
    0x0c, // 0x03
    0x02, // 0x04
    0x0a, // 0x05
    0x06, // 0x06
    0x0e, // 0x07
    0x01, // 0x08
    0x09, // 0x09
    0x05, // 0x0a
    0x0d, // 0x0b
    0x03, // 0x0c
    0x0b, // 0x0d
    0x07, // 0x0e
    0x0f, // 0x0f
};

// TODO: Should probably use a MUTEX here.
void set_lna_bias(uint_fast8_t bias) {
    lna_bias_last = bias;
    if(lna_mode_enabled == 1) {
        GPIODirModeSet(GPIO_PORTK_BASE, 0x0f, GPIO_DIR_MODE_IN);
        GPIODirModeSet(GPIO_PORTK_BASE, lna_flip_bits[bias], GPIO_DIR_MODE_OUT);
        GPIOPinWrite(GPIO_PORTK_BASE, lna_flip_bits[bias], lna_flip_bits[bias]);
    }
}

void set_lna_off_mode() {
    lna_mode_enabled = 0;
    GPIODirModeSet(GPIO_PORTK_BASE, 0x0f, GPIO_DIR_MODE_IN);
}

void set_lna_on_mode() {
    lna_mode_enabled = 1;
    GPIODirModeSet(GPIO_PORTK_BASE, 0x0f, GPIO_DIR_MODE_IN);
    GPIODirModeSet(GPIO_PORTK_BASE, lna_flip_bits[lna_bias_last], GPIO_DIR_MODE_OUT);
    GPIOPinWrite(GPIO_PORTK_BASE, lna_flip_bits[lna_bias_last], lna_flip_bits[lna_bias_last]);
}
