/*****************************************************************************
*
* Copyright (C) 2013 - 2017 Texas Instruments Incorporated - http://www.ti.com/
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
*
* * Redistributions of source code must retain the above copyright
*   notice, this list of conditions and the following disclaimer.
*
* * Redistributions in binary form must reproduce the above copyright
*   notice, this list of conditions and the following disclaimer in the
*   documentation and/or other materials provided with the
*   distribution.
*
* * Neither the name of Texas Instruments Incorporated nor the names of
*   its contributors may be used to endorse or promote products derived
*   from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
* A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
* OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
* SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
* LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
* THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************
*
* MSP432 empty main.c template
*
******************************************************************************/

#include <ti/sysbios/BIOS.h>

#include <ti/devices/msp432e4/driverlib/sysctl.h>
#include <ti/devices/msp432e4/driverlib/gpio.h>

#include <ti/devices/msp432e4/inc/msp432e401y.h>

#include "network.h"
#include "temperature.h"

#include "mode.h"

#include "lna.h"
#include "pa.h"

#include "leds.h"
#include "mode_sw.h"
#include "attenuator.h"


#include "msp.h"

int main(void)
{
    volatile int i;
    float temp;
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPION);
    GPIODirModeSet(GPIO_PORTN_BASE, 0x02, GPIO_DIR_MODE_OUT);
    GPIOPadConfigSet(GPIO_PORTN_BASE, 0x02, GPIO_STRENGTH_8MA, GPIO_PIN_TYPE_STD);

    setup_mode();
    setup_lna();
    setup_pa();
    setup_leds();
    setup_mode_sw();
    setup_attenuator();
    setup_temperature();

    //set_mode_sw(1);

    set_lna_power_state(0);
    //set_lna_voltage(3);
    //set_lna_bias(7);

    set_pa_power_state(0);
    //set_pa_voltage(3);
    //set_pa_enable(1);

    for(i=0; i<5000000; ++i);

    //set_attenuator(0, 0x02);
    //set_attenuator(1, 0x0);

    temp = get_temperature_celsius();
    network_start();

//    uart_start();

    set_led_state(0, 1);

    BIOS_start();

    return 0;
}
