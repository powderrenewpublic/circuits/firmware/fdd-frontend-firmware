/*
 * ssi2.c
 *
 *  Created on: Nov 8, 2019
 *      Author: alex
 */



#include <ti/devices/msp432e4/driverlib/sysctl.h>
#include <ti/devices/msp432e4/driverlib/gpio.h>
#include <ti/devices/msp432e4/driverlib/ssi.h>

#include <ti/drivers/dpl/ClockP.h>

#include <ti/devices/msp432e4/inc/msp432e401y.h>

#include <ti/devices/msp432e4/driverlib/pin_map.h>

uint8_t ssi2_setup_complete = 0;

void setup_ssi2() {
    ClockP_FreqHz freq;

    if(ssi2_setup_complete) {
        return;
    }

    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_SSI2);

    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_SSI2));

    ClockP_getCpuFreq(&freq);

    // Setup SSI
    GPIOPinConfigure(GPIO_PD0_SSI2XDAT1);
    GPIOPinConfigure(GPIO_PD1_SSI2XDAT0);
    GPIOPinConfigure(GPIO_PD3_SSI2CLK);
    GPIOPinTypeSSI(GPIO_PORTD_BASE, 0x0b);
    SSIConfigSetExpClk(SSI2_BASE, freq.lo, SSI_FRF_MOTO_MODE_0,
                       SSI_MODE_MASTER, 2000000, 8);
    SSIEnable(SSI2_BASE);

    ssi2_setup_complete = 1;
}
