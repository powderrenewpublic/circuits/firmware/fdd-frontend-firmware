/*
 * mode.h
 *
 *  Created on: Nov 24, 2020
 *      Author: alex
 */

#ifndef MODE_H_
#define MODE_H_

#include <stdint.h>



void setup_mode();

void set_mode(uint_fast8_t mode);
uint_fast8_t get_mode();

#endif /* MODE_H_ */
