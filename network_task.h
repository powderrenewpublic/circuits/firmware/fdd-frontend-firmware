/*
 * network_task.h
 *
 *  Created on: Nov 24, 2020
 *      Author: alex
 */

#ifndef NETWORK_TASK_H_
#define NETWORK_TASK_H_


void listener_thread(struct listener_info_s* listener_info,
                              char* connection_thread_name);

extern struct listener_info_s tcp_echo_listener;
extern struct listener_info_s console_listener;
extern struct listener_info_s temperature_listener;


#endif /* NETWORK_TASK_H_ */
