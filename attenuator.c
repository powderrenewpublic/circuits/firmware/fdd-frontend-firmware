/*
 * attenuator.c
 *
 *  Created on: Nov 8, 2019
 *      Author: alex
 */



#include <ti/devices/msp432e4/driverlib/sysctl.h>
#include <ti/devices/msp432e4/driverlib/gpio.h>
#include <ti/devices/msp432e4/driverlib/ssi.h>

#include <ti/devices/msp432e4/inc/msp432e401y.h>

#include <ti/devices/msp432e4/driverlib/pin_map.h>

#include "ssi2.h"

void setup_attenuator() {
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);

    // CS GPIO
    GPIODirModeSet(GPIO_PORTD_BASE, 0x04, GPIO_DIR_MODE_OUT);
    GPIOPadConfigSet(GPIO_PORTD_BASE, 0x04, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD);
    //GPIOPinWrite(GPIO_PORTD_BASE, 0x04, 0x04);
    GPIOPinWrite(GPIO_PORTD_BASE, 0x00, 0x00);

    setup_ssi2();
}

static void att_cs_en() {
    //GPIOPinWrite(GPIO_PORTD_BASE, 0x04, 0x0);
}

static void att_cs_dis() {
    volatile int i;
    GPIOPinWrite(GPIO_PORTD_BASE, 0x04, 0x04);
    for(i=0; i<255; ++i);
    GPIOPinWrite(GPIO_PORTD_BASE, 0x04, 0x0);
}

void set_attenuator(uint_fast8_t rx, uint_fast8_t value) {
    uint32_t input_data;
    att_cs_en();
    SSIDataPut(SSI2_BASE, value);
    SSIDataGet(SSI2_BASE, &input_data);
    SSIDataPut(SSI2_BASE, rx ? 0x80 : 0x00);
    SSIDataGet(SSI2_BASE, &input_data);
    att_cs_dis();
}
