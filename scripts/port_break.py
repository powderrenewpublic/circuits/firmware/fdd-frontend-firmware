import socket
import time

fe_ip = '10.42.0.154'
fe_port = 451

def make_test_close_socket():
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.settimeout(4)
        s.connect((fe_ip, fe_port))
        return True

    return False

count = 0
while make_test_close_socket():
    time.sleep(0.001)
    print(f'count: {count}')
    count += 1
