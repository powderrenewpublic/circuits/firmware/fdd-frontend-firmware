/*
 * lna.c
 *
 *  Created on: Sep 6, 2019
 *      Author: Alex Orange
 */

#include <ti/devices/msp432e4/driverlib/sysctl.h>
#include <ti/devices/msp432e4/driverlib/gpio.h>

#include <ti/devices/msp432e4/inc/msp432e401y.h>

uint_fast8_t pa_enabled_last;
uint_fast8_t pa_mode_enabled;

void setup_pa() {
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPION);

    // Power supply
    GPIODirModeSet(GPIO_PORTN_BASE, 0x3a, GPIO_DIR_MODE_OUT);
    GPIODirModeSet(GPIO_PORTN_BASE, 0x05, GPIO_DIR_MODE_IN);
    GPIOPadConfigSet(GPIO_PORTN_BASE, 0x3a, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD);
    GPIOPinWrite(GPIO_PORTN_BASE, 0x08, 0x08);

    // Enable pin
    pa_enabled_last = 1;
    pa_mode_enabled = 1;
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOH);
    GPIODirModeSet(GPIO_PORTH_BASE, 0x01, GPIO_DIR_MODE_OUT);
    GPIOPadConfigSet(GPIO_PORTH_BASE, 0x01, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD);
    GPIOPinWrite(GPIO_PORTH_BASE, 0x01, 0x01);
}

void set_pa_power_state(uint_fast8_t on) {
    GPIOPinWrite(GPIO_PORTN_BASE, 0x20, on ? 0x20 : 0x00);
}

void set_pa_voltage(uint_fast8_t value) {
    GPIOPinWrite(GPIO_PORTN_BASE, 0x10, (value & 0x01) ? 0x10 : 0x00);
    GPIOPinWrite(GPIO_PORTN_BASE, 0x02, (value & 0x02) ? 0x02 : 0x00);
}

uint_fast8_t get_pa_power_good() {
    return GPIOPinRead(GPIO_PORTN_BASE, 0x05);
}

// TODO: Should probably use a MUTEX here.
void set_pa_enable(uint_fast8_t enabled) {
    pa_enabled_last = enabled;
    if(pa_mode_enabled == 1) {
        GPIOPinWrite(GPIO_PORTH_BASE, 0x01, enabled ? 0x01 : 0x00);
    }
}

void set_pa_off_mode() {
    pa_mode_enabled = 0;
    GPIOPinWrite(GPIO_PORTH_BASE, 0x01, 0x00);
}

void set_pa_on_mode() {
    pa_mode_enabled = 1;
    GPIOPinWrite(GPIO_PORTH_BASE, 0x01, pa_enabled_last ? 0x01 : 0x00);
}
