/*
 * mac_addr.c
 *
 *  Created on: Nov 8, 2019
 *      Author: alex
 */

#include <ti/devices/msp432e4/driverlib/sysctl.h>
#include <ti/devices/msp432e4/driverlib/gpio.h>
#include <ti/devices/msp432e4/driverlib/ssi.h>

#include <ti/devices/msp432e4/inc/msp432e401y.h>

#include <ti/devices/msp432e4/driverlib/pin_map.h>

#include "ssi2.h"

void setup_mac_addr() {
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);

    // CS GPIO
    GPIODirModeSet(GPIO_PORTD_BASE, 0x40, GPIO_DIR_MODE_OUT);
    GPIOPadConfigSet(GPIO_PORTD_BASE, 0x40, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD);
    GPIOPinWrite(GPIO_PORTD_BASE, 0x40, 0x40);

    setup_ssi2();
}

static void mac_cs_en() {
    GPIOPinWrite(GPIO_PORTD_BASE, 0x40, 0x0);
}

static void mac_cs_dis() {
    GPIOPinWrite(GPIO_PORTD_BASE, 0x40, 0x40);
}

extern unsigned char macAddress[6];

void get_mac_addr() {
    uint32_t input_data;

    mac_cs_en();
    SSIDataPut(SSI2_BASE, 0x03);
    SSIDataPut(SSI2_BASE, 0xfa);
    // First two bytes are trash (Hi-Z)
    SSIDataGet(SSI2_BASE, &input_data);
    SSIDataGet(SSI2_BASE, &input_data);
    //
    SSIDataPut(SSI2_BASE, 0);
    SSIDataGet(SSI2_BASE, &input_data);
    macAddress[0] = (unsigned char)(input_data & 0xff);
    SSIDataPut(SSI2_BASE, 0);
    SSIDataGet(SSI2_BASE, &input_data);
    macAddress[1] = (unsigned char)(input_data & 0xff);
    SSIDataPut(SSI2_BASE, 0);
    SSIDataGet(SSI2_BASE, &input_data);
    macAddress[2] = (unsigned char)(input_data & 0xff);
    SSIDataPut(SSI2_BASE, 0);
    SSIDataGet(SSI2_BASE, &input_data);
    macAddress[3] = (unsigned char)(input_data & 0xff);
    SSIDataPut(SSI2_BASE, 0);
    SSIDataGet(SSI2_BASE, &input_data);
    macAddress[4] = (unsigned char)(input_data & 0xff);
    SSIDataPut(SSI2_BASE, 0);
    SSIDataGet(SSI2_BASE, &input_data);
    macAddress[5] = (unsigned char)(input_data & 0xff);
    mac_cs_dis();
}
