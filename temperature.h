/*
 * temperature.h
 *
 *  Created on: Sep 5, 2019
 *      Author: crazycasta
 */

#ifndef TEMPERATURE_H_
#define TEMPERATURE_H_



void setup_temperature();
float get_temperature_celsius();
void temperature_thread(int16_t connection_socket, uintptr_t unused);


#endif /* TEMPERATURE_H_ */
