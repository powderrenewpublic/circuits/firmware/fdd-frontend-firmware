/*
 * network.c
 *
 *  Created on: Sep 5, 2019
 *      Author: Alex Orange
 */

//#include <ti/ndk/inc/netmain.h>
#include <stdint.h>
#include <ti/ndk/inc/os/oskern.h>


#include <signal.h>
#include <time.h>

//#include <arpa/inet.h>

#include <ti/devices/msp432e4/driverlib/gpio.h>

#include <ti/devices/msp432e4/inc/msp432e401y.h>

#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>

#include <ti/ndk/inc/netmain.h>
#include <ti/ndk/slnetif/slnetifndk.h>
#include <ti/net/slnetif.h>

#include "console.h"
#include "temperature.h"
#include "mac_addr.h"

#include "network_task.h"


/* Socket file descriptor table */
#define MAXSOCKETS 10
uint32_t ti_ndk_socket_max_fd = MAXSOCKETS;
void *ti_ndk_socket_fdtable[MAXSOCKETS];

/* NDK memory manager page size and number of pages [used by mmAlloc()] */
#define RAW_PAGE_SIZE 3072
#define RAW_PAGE_COUNT 6

const int ti_ndk_config_Global_rawPageSize  = RAW_PAGE_SIZE;
const int ti_ndk_config_Global_rawPageCount = RAW_PAGE_COUNT;

/* P.I.T. (page information table) */
#pragma DATA_SECTION(ti_ndk_config_Global_pit, ".bss:NDK_MMBUFFER");
#pragma DATA_SECTION(ti_ndk_config_Global_pitBuffer, ".bss:NDK_MMBUFFER");
PITENTRY ti_ndk_config_Global_pit[RAW_PAGE_COUNT];
unsigned char ti_ndk_config_Global_pitBuffer[RAW_PAGE_SIZE * RAW_PAGE_COUNT];

/* Memory bucket sizes */
#define SMALLEST 48
#define LARGEST (RAW_PAGE_SIZE)

const int ti_ndk_config_Global_smallest = SMALLEST;
const int ti_ndk_config_Global_largest  = LARGEST;

/* Memory Slot Tracking */
uint32_t ti_ndk_config_Global_Id2Size[] =
        {SMALLEST, 96, 128, 256, 512, 1536, LARGEST};

/*
 *  Local Packet Buffer Pool Definitions
 *
 *  The below variables/defines are used to override the defaults that are set
 *  in the Packet Buffer Manager (PBM) file src/stack/pbm/pbm_data.c
 */

/*
 *  Number of buffers in PBM packet buffer free pool
 *
 *  The number of buffers in the free pool can have a significant effect
 *  on performance, especially in UDP packet loss. Increasing this number
 *  will increase the size of the static packet pool use for both sending
 *  and receiving packets.
 */
#define PKT_NUM_FRAMEBUF 10

/* Size of Ethernet frame buffer */
#define PKT_SIZE_FRAMEBUF   1536

const int ti_ndk_config_Global_numFrameBuf = PKT_NUM_FRAMEBUF;
const int ti_ndk_config_Global_sizeFrameBuf = PKT_SIZE_FRAMEBUF;

#pragma DATA_ALIGN(ti_ndk_config_Global_pBufMem, 128);
#pragma DATA_SECTION(ti_ndk_config_Global_pBufMem, ".bss:NDK_PACKETMEM");
unsigned char
        ti_ndk_config_Global_pBufMem[PKT_NUM_FRAMEBUF * PKT_SIZE_FRAMEBUF];


#pragma DATA_ALIGN(ti_ndk_config_Global_pHdrMem, 128);
#pragma DATA_SECTION(ti_ndk_config_Global_pHdrMem, ".bss:NDK_PACKETMEM");
unsigned char ti_ndk_config_Global_pHdrMem[PKT_NUM_FRAMEBUF * sizeof(PBM_Pkt)];





extern void llTimerTick();

void network_setup_heartbeat() {
    int ret;
    timer_t ndk_heart_beat;
    int ndk_heart_beat_count = 0;

    struct sigevent sev = {
        .sigev_notify = SIGEV_SIGNAL,
        .sigev_value = {
            .sival_ptr = &ndk_heart_beat_count
        },
        .sigev_notify_attributes = NULL,
        .sigev_notify_function = &llTimerTick
    };

    struct itimerspec its = {
        .it_interval = {
            .tv_sec = 0,
            .tv_nsec = 100000000
        },
        .it_value = {
            .tv_sec = 0,
            .tv_nsec = 100000000
        }
    };

    ret = timer_create(CLOCK_MONOTONIC, &sev, &ndk_heart_beat);
    if(ret != 0) {
        while(1);
    }

    ret = timer_settime(ndk_heart_beat, 0, &its, NULL);
    if(ret != 0) {
        while(1);
    }
}

void network_setup_dhcp(void * h_cfg) {
    CI_SERVICE_DHCPC dhcpc;
    unsigned char DHCP_OPTIONS[] = { DHCPOPT_SUBNET_MASK };

    memset(&dhcpc, 0, sizeof(dhcpc));
    dhcpc.cisargs.Mode = CIS_FLG_IFIDXVALID;
    dhcpc.cisargs.IfIdx = 1;
    dhcpc.cisargs.pCbSrv = NULL; // Callback for when DHCP updates stuff
    dhcpc.param.pOptions = DHCP_OPTIONS;
    dhcpc.param.len = 1;
    CfgAddEntry(h_cfg, CFGTAG_SERVICE, CFGITEM_SERVICE_DHCPCLIENT, 0,
                sizeof(dhcpc), (unsigned char*)&dhcpc, NULL);
}

void network_open() {
}

void network_close() {
}

void network_ip_address(uint32_t ip_addr, uint32_t if_idx, uint32_t f_add) {
    int32_t status;

    status = SlNetSock_init(0);
    if(status != 0) {
        while(1);
    }

    status = SlNetIf_init(0);
    if(status != 0) {
        while(1);
    }

    status = SlNetIf_add(SLNETIF_ID_2, "eth0",
                         (const SlNetIf_Config_t*)&SlNetIfConfigNDK, 4);

    TaskCreate(listener_thread,
                             "tcp_echo_listener_thread", OS_TASKPRINORM, 2048,
                             &tcp_echo_listener, "tcp_echo_connection_thread",
                             0);
    TaskCreate(listener_thread,
                             "console_listener_thread", OS_TASKPRINORM, 2048,
                             &console_listener, "console_connection_thread",
                             0);
    TaskCreate(listener_thread,
                             "temperature_listener_thread", OS_TASKPRINORM,
                             2048, &temperature_listener,
                             "temperature_connection_thread", 0);
}

void network_setup_tcp(void * h_cfg) {
    int transmit_buffer_size = 1024;
    int receive_buffer_size = 1024;
    int receive_buffer_limit = 2048;

    CfgAddEntry(h_cfg, CFGTAG_IP, CFGITEM_IP_SOCKTCPTXBUF, CFG_ADDMODE_UNIQUE,
                sizeof(uint32_t), (unsigned char *)&transmit_buffer_size,
                NULL);
    CfgAddEntry(h_cfg, CFGTAG_IP, CFGITEM_IP_SOCKTCPRXBUF, CFG_ADDMODE_UNIQUE,
                sizeof(uint32_t), (unsigned char *)&receive_buffer_size,
                NULL);
    CfgAddEntry(h_cfg, CFGTAG_IP, CFGITEM_IP_SOCKTCPRXLIMIT,
                CFG_ADDMODE_UNIQUE, sizeof(uint32_t),
                (unsigned char *)&receive_buffer_limit, NULL);
}

void network_thread(uintptr_t arg0, uintptr_t arg1) {
    void * h_cfg;
    int ret;
    network_setup_heartbeat();

    ret = NC_SystemOpen(NC_PRIORITY_LOW, NC_OPMODE_INTERRUPT);
    if(ret != 0) {
        while(1);
    }

    h_cfg = CfgNew();

    network_setup_dhcp(h_cfg);
    network_setup_tcp(h_cfg);

    do {
        GPIOPinWrite(GPIO_PORTN_BASE, 0x02, 0x02);
        ret = NC_NetStart(h_cfg, network_open, network_close,
                          network_ip_address);
    } while(ret > 0);

    CfgFree(h_cfg);
    NC_SystemClose();
}

void network_start() {
    Task_Params params;
    Task_Handle network_thread_handle;

    setup_mac_addr();
    get_mac_addr();

    Task_Params_init(&params);

    params.instance->name = "networking_thread";
    params.priority = 5;
    params.stackSize = 2048;

    network_thread_handle = Task_create((Task_FuncPtr)network_thread,
                                        &params, NULL);

    if(network_thread_handle == 0) {
        while(1);
    }
}
