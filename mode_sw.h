/*
 * mode_sw.h
 *
 *  Created on: Nov 8, 2019
 *      Author: alex
 */

#ifndef MODE_SW_H_
#define MODE_SW_H_

void setup_mode_sw();
void set_mode_sw(uint_fast8_t ue);

#endif /* MODE_SW_H_ */
