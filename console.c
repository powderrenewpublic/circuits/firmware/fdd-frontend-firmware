/*
 * console.c
 *
 *  Created on: Sep 8, 2019
 *      Author: Alex Orange
 */

#include <stdint.h>

#include "mode.h"
#include "lna.h"
#include "pa.h"
#include "leds.h"
#include "attenuator.h"

#include <sys/socket.h>


// Yep... this is how they do it in the tcpEcho example...
extern void fdOpenSession();
extern void fdCloseSession();
extern void *TaskSelf();


void console_task(int16_t connection_socket, uintptr_t unused) {
    char recv_queue[1];
    char out_queue[1];
    int recv_size;
    char num;
    char num2;
    char query = 0;

    fdOpenSession(TaskSelf());

    while((recv_size = recv(connection_socket, recv_queue,
                            sizeof(recv_queue), 0)) > 0) {
        switch(recv_queue[0]) {
            case 'l':
                set_lna_power_state(num & 1);
                set_lna_voltage(num & 2);
                break;
            case 'L':
                set_lna_bias(num);
                break;
            case 'i':
                set_led_state(num & 0x3, num & 0x4);
                break;
            case 'p':
                set_pa_power_state(num & 1);
                set_pa_voltage(num & 2);
                break;
            case 'P':
                set_pa_enable(num);
                break;
            case 'x':
                num2 = num;
                break;
            case 't':
                set_attenuator(0, num | num2 << 4);
                break;
            case 'T':
                set_attenuator(1, num | num2 << 4);
                break;
            case 'm':
                if(query != 0) {
                    out_queue[0] = get_mode() + '0';
                    send(connection_socket, out_queue, 1, 0);
                } else {
                    if(!(num >= 0 && num <= 2)) {
                        send(connection_socket, "@#%$die!!", 9, 0);
                        close(connection_socket);
                    } else {
                        set_mode(num);
                    }
                }
                break;
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                num = recv_queue[0] - '0';
                query = 0;
                break;
            case 'a':
            case 'b':
            case 'c':
            case 'd':
            case 'e':
            case 'f':
                num = recv_queue[0] - 'W';
                query = 0;
                break;
            case 'A':
            case 'B':
            case 'C':
            case 'D':
            case 'E':
            case 'F':
                num = recv_queue[0] - '7';
                query = 0;
                break;
            case '?':
                query = 1;
                break;
        }

        send(connection_socket, recv_queue, recv_size, 0);
    }

    close(connection_socket);
    fdCloseSession(TaskSelf());
}
