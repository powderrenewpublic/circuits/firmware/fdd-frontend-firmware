/*
 * pa.h
 *
 *  Created on: Sep 6, 2019
 *      Author: Alex Orange
 */

#ifndef PA_H_
#define PA_H_

#include <stdint.h>



void setup_pa();
void set_pa_power_state(uint_fast8_t on);
void set_pa_voltage(uint_fast8_t high);
uint_fast8_t get_pa_power_good();
void set_pa_enable(uint_fast8_t enabled);

// These two messages are global overrides to shut off FDD mode
// Set on will only set on if the last set_pa_enable state was true
void set_pa_off_mode();
void set_pa_on_mode();


#endif /* PA_H_ */
