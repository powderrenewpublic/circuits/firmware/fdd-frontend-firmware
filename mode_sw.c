/*
 * mode_sw.c
 *
 *  Created on: Nov 8, 2019
 *      Author: alex
 */


#include <ti/devices/msp432e4/driverlib/sysctl.h>
#include <ti/devices/msp432e4/driverlib/gpio.h>

#include <ti/devices/msp432e4/inc/msp432e401y.h>


void setup_mode_sw() {
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOP);

    // GPIO
    GPIODirModeSet(GPIO_PORTP_BASE, 0x30, GPIO_DIR_MODE_OUT);
    GPIOPadConfigSet(GPIO_PORTP_BASE, 0x30, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD);
    GPIOPinWrite(GPIO_PORTP_BASE, 0x30, 0x20);
}

void set_mode_sw(uint_fast8_t ue) {
    GPIOPinWrite(GPIO_PORTP_BASE, 0x30, ue ? 0x20 : 0x10);
}
