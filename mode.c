/*
 * mode.c
 *
 *  Created on: Nov 24, 2020
 *      Author: alex
 */

#include <ti/devices/msp432e4/driverlib/sysctl.h>
#include <ti/devices/msp432e4/driverlib/gpio.h>

#include <ti/devices/msp432e4/inc/msp432e401y.h>

#include <ti/sysbios/knl/Task.h>


#include "mode.h"
#include "lna.h"
#include "pa.h"

uint_fast8_t mode;

void setup_mode() {
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOM);

    // Mode pin TDD on = 0 high
    mode = 0;
    GPIODirModeSet(GPIO_PORTM_BASE, 0x01, GPIO_DIR_MODE_OUT);
    GPIOPadConfigSet(GPIO_PORTM_BASE, 0x01, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD);
    GPIOPinWrite(GPIO_PORTM_BASE, 0x01, 0x00);
}

void set_mode(uint_fast8_t _mode) {
    mode = _mode;

    // Turn off PA if going into TDD mode
    if(_mode == 1) {
        set_pa_off_mode();
        set_lna_off_mode();
    }

    // Turn on/off TDD
    switch(_mode) {
        case 1:
            GPIOPinWrite(GPIO_PORTM_BASE, 0x01, 0x01);
            break;
        case 0:
        case 2:
            GPIOPinWrite(GPIO_PORTM_BASE, 0x01, 0x00);
            break;
    }

    Task_sleep(1);

    // Turn on our stuff
    switch(_mode) {
        case 0:
            set_pa_on_mode();
            set_lna_on_mode();
            break;
        case 1:
        case 2:
            set_pa_off_mode();
            set_lna_off_mode();
            break;
    }
}

uint_fast8_t get_mode() {
    return mode;
}
