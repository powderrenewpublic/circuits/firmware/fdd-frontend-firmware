/*
 * temperature.c
 *
 *  Created on: Sep 5, 2019
 *      Author: Alex Orange
 */


#include <stdint.h>
#include <stdio.h>


#include <sys/socket.h>


#include <ti/sysbios/knl/Task.h>

#include <ti/devices/msp432e4/driverlib/sysctl.h>
#include <ti/devices/msp432e4/driverlib/adc.h>

#include <ti/devices/msp432e4/inc/msp432e401y.h>


// Yep... this is how they do it in the tcpEcho example...
extern void fdOpenSession();
extern void fdCloseSession();
extern void *TaskSelf();


void setup_temperature() {
    SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);

    ADCSequenceConfigure(ADC0_BASE, 3, ADC_TRIGGER_PROCESSOR, 0);
    ADCSequenceStepConfigure(ADC0_BASE, 3, 0,
                             ADC_CTL_TS | ADC_CTL_IE | ADC_CTL_END);
    ADCSequenceEnable(ADC0_BASE, 3);
    ADCIntClear(ADC0_BASE, 3);
}

int32_t get_temperature_raw() {
    uint32_t adc_value;

    ADCProcessorTrigger(ADC0_BASE, 3);
    while(!ADCIntStatus(ADC0_BASE, 3, false));
    ADCIntClear(ADC0_BASE, 3);

    ADCSequenceDataGet(ADC0_BASE, 3, &adc_value);

    return adc_value;
}

float get_temperature_celsius() {
    return ((3.3/4096*get_temperature_raw()) - 1.633) / -0.0133 + 25.0;
}

void temperature_thread(int16_t connection_socket, uintptr_t unused) {
    char buffer[64];
    size_t string_length;

    fdOpenSession(TaskSelf());

    do {
        Task_sleep(1000);
        string_length = snprintf(buffer, sizeof(buffer), "%3.6f\n", get_temperature_celsius());
    } while(send(connection_socket, buffer, string_length, 0) > 0);

    close(connection_socket);
    fdCloseSession(TaskSelf());
}
